<?php

namespace Drupal\custom_customers\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for customer entities.
 */
class CustomerViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
