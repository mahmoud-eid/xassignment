<?php

namespace Drupal\custom_customers\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting customer entities.
 *
 * @ingroup custom_customers
 */
class CustomerDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    $url = Url::fromRoute('view.customer.page_customers');
    $form_state->setRedirectUrl($url);

    $this->messenger()->addMessage($this->getDeletionMessage());
  }

}
