<?php

namespace Drupal\custom_customers;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of customer entities.
 *
 * @ingroup custom_customers
 */
class CustomerListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Customer ID');
    $header['name'] = $this->t('Customer Name');
    $header['balance'] = $this->t('Balance');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\custom_customers\Entity\Customer $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.customer.edit_form',
      ['customer' => $entity->id()]
    );
    $row['balance'] = $entity->getBalance();
    return $row + parent::buildRow($entity);
  }

}
