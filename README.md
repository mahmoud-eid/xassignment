# Xassignment
## Setup instructions

### Step #1: Docksal environment setup

**This is a one time setup - skip this if you already have a working Docksal environment.**

Follow [Docksal environment setup instructions](https://docs.docksal.io/en/master/getting-started/env-setup)

### Step #2: Project setup

1. Clone this repo into your Projects directory:

    ```
    git clone https://mahmoud-eid@bitbucket.org/mahmoud-eid/xassignment.git xassignment
    ```
    
    ```
    cd xassignment
    ```

2. Start docksal:

    ```
    fin up
    ```

3. Download the database then import it:

    [**https://bitbucket.org/mahmoud-eid/xassignment/downloads/dev_xassignment.sql**](https://bitbucket.org/mahmoud-eid/xassignment/downloads/dev_xassignment.sql)

    ```
    fin sqli /PATH/TO/dev_xassignment.sql
    ```

4. Clear the cache:

    ```
    fin drush cr
    ```

5. Point your browser to

    ```
    http://xassignment.docksal
    ```

Use the following credentials to login:

    username: admin

    password: admin

## CSV Migration

### Step #1: Prepare the file:

1. Go to xassignment/web/sites/default/files folder and create new folder and name it "import".

2. Download customers.csv example and place it at import folder:

  [**https://bitbucket.org/mahmoud-eid/xassignment/downloads/customers.csv**](https://bitbucket.org/mahmoud-eid/xassignment/downloads/customers.csv)

So the path to the file must be like: sites/default/files/import/customers.csv

### Step #2: Run the migration:

1. The migration is configured to run every hour by cron job, please check the configuration URL:

    [**http://xassignment.docksal/admin/config/system/cron/jobs**](http://xassignment.docksal/admin/config/system/cron/jobs)

2. However you can run it also by drush:

    ```
    fin drush migrate:import customers
    ```
    
    ```
    fin drush migrate:rollback customers
    ```
    
    ```
    fin drush migrate:stop customers
    ```
    
    ```
    fin drush migrate:reset-status customers
    ```

## Rest API

For simplisty you can **download and import Postman collection and enviroment** from project downloads folder:

  - [**https://bitbucket.org/mahmoud-eid/xassignment/downloads/ImageX_Customers.postman_collection.json**](https://bitbucket.org/mahmoud-eid/xassignment/downloads/ImageX_Customers.postman_collection.json)

  - [**https://bitbucket.org/mahmoud-eid/xassignment/downloads/ImageX_DEV_env.postman_environment.json**](https://bitbucket.org/mahmoud-eid/xassignment/downloads/ImageX_DEV_env.postman_environment.json)

**Then you can just click send at every request but kindly note to cheange the UUID for update and delete requestes to make sure it's exist.**

1. Generate Bearer token:
    
    Method: POST

    URL: http://xassignment.docksal/oauth/token
    
    Body: form-data
    
    Request Parameters:
    
      - grant_type: client_credentials
      
      - client_id
      
      - client_secret
      
      - username
      
      - password

2. Customers collection:
    
    Method: GET
    
    URL: http://xassignment.docksal/jsonapi/customers
    
    Headers:
    
      - Accept: application/vnd.api+json
      
      - Content-Type: application/vnd.api+json
      
      - Authorization: Bearer token that you generated at step 1.

3. X-CSRF-Token:
    
    Method: GET
    
    URL: http://xassignment.docksal/session/token

4. Create customer:
    
    Method: POST
    
    URL: http://xassignment.docksal/jsonapi/customers
    
    Headers:
    
      - Accept: application/vnd.api+json
      
      - Content-Type: application/vnd.api+json
      
      - X-CSRF-Token: ou must validate your request by adding X-CSRF-Token to the request header and set its value from the response that you generated at step 3.
      
      - Authorization: Bearer token that you generated at step 1.

    Body ex:

    ```
    {
      "data": {
        "type": "customers--customer",
        "attributes": {
        "name": "Customer name",
        "balance": 300
        }
      }
    }
    ```

5. Update customer:
    
    Method: PATCH
    
    URL: http://xassignment.docksal/jsonapi/customers/{UUID}
    
    Headers:
    
      - Accept: application/vnd.api+json
      
      - Content-Type: application/vnd.api+json
      
      - X-CSRF-Token: ou must validate your request by adding X-CSRF-Token to the request header and set its value from the response that you generated at step 3.
      
      - Authorization: Bearer token that you generated at step 1.

    Body ex:
    
    ```
    {
      "data": {
        "type": "customers--customer",
        "id": {UUID},
        "attributes": {
          "name": "My new name"
        }
      }
    }
    ```

6. Delete customer:
    
    Method: DELETE
    
    URL: http://xassignment.docksal/jsonapi/customers/{UUID}
    
    Headers:
      - Accept: application/vnd.api+json
      
      - Content-Type: application/vnd.api+json
      
      - X-CSRF-Token: ou must validate your request by adding X-CSRF-Token to the request header and set its value from the response that you generated at step 3.
      
      - Authorization: Bearer token that you generated at step 1.

## Note:

It's not secure to push public and private certifications but I made this to make the assignment easy to test, also, it's recommend to change key folder permission -R to 600 or 660. 